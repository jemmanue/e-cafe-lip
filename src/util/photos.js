export const allPhotos = [
    {
        author: "Ludovic",
        description: "Sainte Victoire",
        slug: "ludo_ste_victoire"
    },
    {
        author: "Ludovic",
        description: "Plan de Prals",
        slug: "ludo_plan_prals"
    },
    {
        author: "Ludovic",
        description: "Lis turban",
        slug: "ludo_lis_turban"
    },
    {
        author: "Jean-Michel",
        slug: "jm_mer_crepuscule"
    },
    {
        author: "Jean-Michel",
        slug: "jm_papillons_cadre"
    },
    {
        author: "Matthieu",
        slug: "matthieu_mouche"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleurs_jaunes"
    },
    {
        author: "Sylvie",
        slug: "sylvie_feuille_rouge"
    },
    {
        author: "Sylvie",
        slug: "sylvie_temple"
    },
    {
        author: "Sylvie",
        slug: "sylvie_muguet"
    },
    {
        author: "Jean-Michel",
        description: "Ânes",
        slug: "jm_anes_hdr"
    },
    {
        author: "Sylvie",
        slug: "sylvie_coquelicots"
    },
    {
        author: "Ludovic",
        description: "Gesse",
        slug: "ludo_gesse"
    },
    {
        author: "Ludovic",
        slug: "ludo_panorama"
    },
    {
        author: "Jean-Michel",
        slug: "jm_feuilles_gouttes"
    },
    {
        author: "Jean-Michel",
        slug: "jm_champ_nb"
    },
    {
        author: "Ludovic",
        description: "Gorges du Loup",
        slug: "ludo_gorges_loup"
    },
    {
        author: "Sylvie",
        description: "Flamants roses",
        slug: "sylvie_flamants_roses"
    },
    {
        author: "Ludovic",
        description: "Aconit napelle",
        slug: "ludo_aconit_napelle"
    },
    {
        author: "Sylvie",
        slug: "sylvie_champignons"
    },
    {
        author: "Jean-Michel",
        slug: "jm_branches_arbres"
    },
    {
        author: "Ludovic",
        description: "Vallon de Cougourde",
        slug: "ludo_vallon_cougourde"
    },
    {
        author: "Jean-Michel",
        slug: "jm_foret_automne"
    },
    {
        author: "Ludovic",
        slug: "ludo_fleurs_blanches_2"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleurs_bleues"
    },
    {
        author: "Jean-Michel",
        slug: "jm_petite_fleur"
    },
    {
        author: "Jean-Michel",
        slug: "jm_mer_gens"
    },
    {
        author: "Jean-Michel",
        slug: "jm_foret_jogging"
    },
    {
        author: "Ludovic",
        description: "Clapier grand capelet",
        slug: "ludo_clapier_gd_capelet"
    },
    {
        author: "Ludovic",
        description: "Lis orangé",
        slug: "ludo_lis_orange_2"
    },
    {
        author: "Jean-Michel",
        slug: "jm_chateau"
    },
    {
        author: "Sylvie",
        slug: "sylvie_pissenlits"
    },
    {
        author: "Ludovic",
        description: "Lac et grand cimon de rabuons",
        slug: "ludo_lac_grand_cimon_rabuons"
    },
    {
        author: "Jean-Michel",
        slug: "jm_fleurs_jaunes_2"
    },
    {
        author: "Jean-Michel",
        description: "Autrans",
        slug: "jm_autrans"
    },
    {
        author: "Ludovic",
        description: "Col de Longet",
        slug: "ludo_col_longet"
    },
    {
        author: "Ludovic",
        description: "Pivoines",
        slug: "ludo_pivoines"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleurs_blanches"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleurs"
    },
    {
        author: "Sylvie",
        description: "Hérisson",
        slug: "sylvie_herisson"
    },
    {
        author: "Jean-Michel",
        slug: "jm_foret_lumiere_2"
    },
    {
        author: "Ludovic",
        description: "Linaire des Alpes",
        slug: "ludo_linaire_alpes"
    },
    {
        author: "Ludovic",
        description: "Col marinet, pics de la font sancte",
        slug: "ludo_col_marinet_pics_font_sancte"
    },
    {
        author: "Sylvie",
        description: "Bouquet du week-end",
        slug: "sylvie_bouquet"
    },
    {
        author: "Jean-Michel",
        description: "Fleur pas du Vercors",
        slug: "jm_fleur_pas_vercors"
    },
    {
        author: "Jean-Michel",
        slug: "jm_colorado"
    },
    {
        author: "Ludovic",
        description: "Rustrel - Luberon - Colorado",
        slug: "ludo_colorado_rustrel_2"
    },
    {
        author: "Ludovic",
        description: "Rustrel - Luberon - Colorado",
        slug: "ludo_colorado_rustrel"
    },
    {
        author: "Jean-Michel",
        slug: "jm_fleurs_jaunes"
    },
    {
        author: "Ludovic",
        description: "Montagne ennuagée",
        slug: "ludo_montagne_nuages"
    },
    {
        author: "Ludovic",
        description: "Vers le col de Sencours",
        slug: "ludo_col_sencours"
    },
    {
        author: "Jean-Michel",
        description: "Rapace et pont dans les nuages",
        slug: "jm_nb_paysage_rapace"
    },
    {
        author: "Jean-Michel",
        slug: "jm_nb_paysage"
    },
    {
        author: "Ludovic",
        description: "Utelle",
        slug: "ludo_utelle"
    },
    {
        author: "Ludovic",
        description: "Fleur pyramidale",
        slug: "ludo_orchis_pyramidal"
    },
    {
        author: "Jean-Michel",
        slug: "jm_fleur_fourmi"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleur_gouttes"
    },
    {
        author: "Jean-Michel",
        slug: "jm_paysage_2"
    },
    {
        author: "Ludovic",
        description: "Bairols",
        slug: "ludo_bairols"
    },
    {
        author: "Ludovic",
        slug: "ludo_gelas"
    },
    {
        author: "Jean-Michel",
        description: "Rayon de lumière dans la forêt",
        slug: "jm_lumiere_foret"
    },
    {
        author: "Jean-Michel",
        description: "Abeille et petite fleurs",
        slug: "jm_abeille"
    },
    {
        author: "Sylvie",
        description: "Fleur de Lantanier",
        slug: "sylvie_lantanier"
    },
    {
        author: "Ludovic",
        description: "Petite fleur jaune en étoile",
        slug: "ludo_petite_fleur_jaune"
    },
    {
        author: "Ludovic",
        description: "Lac rond",
        slug: "ludo_lac_rond"
    },
    {
        author: "Ludovic",
        description: "Dent Parachée",
        slug: "ludo_dent_parachee"
    },
    {
        author: "Ludovic",
        description: "Marmotte dans un cours d'eau",
        slug: "ludo_marmotte_riviere"
    },
    {
        author: "Jean-Michel",
        slug: "jm_eau_longue_expo"
    },
    {
        author: "Jean-Michel",
        slug: "jm_fleur_rouge"
    },
    {
        author: "Ludovic",
        description: "Fleurs en couple",
        slug: "ludo_fleurs_couple"
    },
    {
        author: "Ludovic",
        slug: "ludo_fleur_rouge"
    },
    {
        author: "Ludovic",
        slug: "ludo_pic_neige"
    },
    {
        author: "Jean-Michel",
        slug: "jm_nb_cadre_neige"
    },
    {
        author: "Sylvie",
        description: "Photo d'Irene ? (Chez Irene peut être ?)",
        slug: "sylvie_irene"
    },
    {
        author: "Ludovic",
        slug: "ludo_fleur_violette"
    },
    {
        author: "Jean-Michel",
        description: "Pas vraiment une fleur",
        slug: "jm_nb_cadre_feuille"
    },
    {
        author: "Ludovic",
        description: "Pivoine (vers Gréolières)",
        slug: "ludo_pivoine_greolieres"
    },
    {
        author: "Sylvie",
        slug: "sylvie_marre"
    },
    {
        author: "Jean-Michel",
        description: "Pétale rose",
        slug: "jm_petale"
    },
    {
        author: "Julien",
        description: "Fleurs roses à la Tête d'Or",
        slug: "julien_fleurs_tete_or"
    },
    {
        author: "Laure",
        description: "Jolies plantes",
        slug: "laure_plantes_multicolores"
    },
    {
        author: "Ludovic",
        description: "Plein de fleurs",
        slug: "ludo_plein_fleurs"
    },
    {
        author: "Sylvie",
        description: "Ibiscus en Crète",
        slug: "sylvie_ibiscus_crete"
    },
    {
        author: "Jean-Michel",
        description: "Fleur blanche",
        slug: "jm_fleur_blanche"
    },
    {
        author: "Ludovic",
        description: "Lever du jour un 2 Janvier",
        slug: "ludo_lever_jour_neige"
    },
    {
        author: "Jean-Michel",
        description: "Lyon",
        slug: "jm_lyon"
    },
    {
        author: "Ludovic",
        description: "Mont blanc ; chamechaude",
        slug: "ludo_chamechaude"
    },
    {
        author: "Matthieu",
        slug: "matthieu_neige"
    },
    {
        author: "Jean-Michel",
        slug: "jm_paysage"
    },
    {
        author: "Ludovic",
        description: "Argenterra et Chambeyron",
        slug: "ludo_argenterra_chambeyron"
    },
    {
        author: "Jean-Michel",
        slug: "jm_arbres"
    },
    {
        author: "Ludovic",
        slug: "ludo_arbre"
    },
    {
        author: "Grégoire",
        description: "Province de Castellon",
        slug: "gregoire_castellon"
    },
    {
        author: "Ludovic",
        description: "Au plan des Noves",
        slug: "ludo_plan_des_noves"
    },
    {
        author: "Jean-Michel",
        slug: "jm_foret_lumiere"
    },
    {
        author: "Ludovic",
        slug: "ludo_fleurs_blanches"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleur_blanche"
    },
    {
        author: "Ludovic",
        slug: "ludo_montagnes"
    },
    {
        author: "Matthieu",
        description: "Gagnant (photo du plus gros animal)",
        slug: "matthieu_elephants"
    },
    {
        author: "Julien",
        description: "Macro-oeil de poney",
        slug: "julien_oeil_bambi"
    },
    {
        author: "Sylvie",
        description: "Concurrent à la photo du plus gros animal",
        slug: "sylvie_gros_animal"
    },
    {
        author: "Ludovic",
        slug: "ludo_coucher_soleil"
    },
    {
        author: "Ludovic",
        slug: "ludo_animal_quadripede"
    },
    {
        author: "Sylvie",
        slug: "sylvie_vaches"
    },
    {
        author: "Jean-Michel",
        slug: "jm_vaches"
    },
    {
        author: "Sylvie",
        slug: "sylvie_fleur_jaune"
    },
    {
        author: "Sylvie",
        slug: "sylvie_papillon"
    },
    {
        author: "Jean-Michel",
        slug: "jm_papillon"
    },
    {
        author: "Ludovic",
        slug: "ludo_lis_orange"
    },
    {
        author: "Ludovic",
        slug: "ludo_fleurs_violettes"
    },
    {
        author: "Jean-Michel",
        slug: "jm_coucher_soleil"
    },
    {
        author: "Jean-Michel",
        description: "Photo interdite aux moins de 18 ans",
        slug: "jm_xinsects"
    },
    {
        author: "Ludovic",
        description: "Lac d'Allos en hiver",
        slug: "ludo_lac_allos_hiver"
    },
    {
        author: "Matthieu",
        slug: "matthieu_coccinelle"
    },
    {
        author: "Jean-Michel",
        slug: "jm_coccinelle"
    },
    {
        author: "Ludovic",
        slug: "ludo_primevere_hirsute"
    },
    {
        author: "Sylvie",
        description: "Bambouseraie d'Anduze",
        slug: "sylvie_bambouseraie_anduze"
    },
    {
        author: "Julien",
        description: "Lac d'Allos plus vieux",
        slug: "julien_lac_allos_vieux"
    },
    {
        author: "Ludovic",
        description: "Lac d'Allos de plus près",
        slug: "ludo_lac_allos_plus_pres"
    },
    {
        author: "Ludovic",
        description: "Lac d'Allos de l'autre côté",
        slug: "ludo_lac_allos"
    },
    {
        author: "Sylvie",
        description: "Ceci est une photo maritime",
        slug: "sylvie_mer"
    },
    {
        author: "Laure",
        description: "Fleurs de cerisier",
        slug: "laure_cerisier"
    },
    {
        author: "Julien",
        description: "Lac d'Allos",
        slug: "julien_lac_allos"
    },
    {
        author: "Sylvie",
        description: "Orchidée ?",
        slug: "sylvie_plante"
    },
    {
        author: "Jean-Michel",
        slug: "jm_lac"
    },
    {
        author: "Jean-Michel",
        description: "Petite fleur du matin",
        slug: "jm_petite_fleur_matin"
    },
    {
        author: "Ludovic",
        description: "Ceci n'est pas une photo maritime",
        slug: "ludo_pas_mer"
    },
    {
        author: "Julien",
        description: "Insecte multicolore inconnu",
        slug: "julien_insecte_multicolore"
    },
    {
        author: "Jean-Michel",
        description: "Forêt",
        slug: "jm_cadre_blanc"
    },
    {
        author: "Sylvie",
        description: "Lilas",
        slug: "sylvie_lilas"
    },
    {
        author: "Jean-Michel",
        description: "Fleurs aquatiques",
        slug: "jm_aquatique"
    },
    {
        author: "Ludovic",
        description: "Aster des Alpes",
        slug: "ludo_aster"
    },
    {
        author: "Jean-Michel",
        description: "Vercors",
        slug: "jm_vercors"
    },
    {
        author: "Christophe",
        description: "Théâtre",
        slug: "christophe_theatre"
    },
    {
        author: "Matthieu",
        description: "Insecte inconnu",
        slug: "matthieu_cest-qui-lui"
    },
    {
        author: "Christophe",
        description: "Fleurs",
        slug: "christophe_fleurs"
    },
    {
        author: "Jean-Michel",
        description: "Petite fleur des champs (Scabieuse suppose Ludo)",
        slug: "jm_scabieuse"
    },
    {
        author: "Ludovic",
        description: "Primevères",
        slug: "ludo_primevere"
    },
    {
        author: "Jean-Michel",
        description: "Fleur rose à Pont-en-Royans",
        slug: "jm_pont-en-royans"
    },
    {
        author: "Jean-Michel",
        description: "Lys près de Gève",
        slug: "jm_lys_geve"
    },
    {
        author: "Sylvie",
        description: "Pensées",
        slug: "sylvie_pensees"
    },
    {
        author: "Ludovic",
        description: "Une aconit napelle au bec de l'orient",
        slug: "ludo_aconit-napelle-au-bec-de-lorient"
    },
    {
        author: "Ludovic",
        description: "Vercors",
        slug: "ludo_vercors"
    },
    {
        author: "Jean-Michel",
        description:
            "Le mont blanc vu depuis les falaises du bout du bout du Vercors",
        slug: "jm_montblanc_vercors"
    },
    {
        author: "Jean-Michel",
        description: "Tite fleur",
        slug: "jm_tite_fleur"
    },
    {
        author: "Sylvie",
        description: "Fleurs oranges",
        slug: "sylvie_fleur_orange"
    },
    {
        author: "Ludovic",
        description: "Papillon blanc",
        slug: "ludo_papillon_blanc"
    },
    {
        author: "Jean-Michel",
        description: "Chenille à La Buffe",
        slug: "jm_chenille"
    },
    {
        author: "Jean-Michel",
        description: "Petites fleurs",
        slug: "jm_petites_fleurs"
    },
    {
        author: "Jean-Michel",
        description: "Fleurs violettes",
        slug: "jm_fleurs_violettes"
    },
    {
        author: "Sylvie",
        description: "Fleur des Pyrénées (possiblement)",
        slug: "sylvie_fleur_pyrenees"
    },
    {
        author: "Ludovic",
        description: "Fleur bizarre",
        slug: "ludo_fleur_bizarre"
    },
    {
        author: "Sylvie",
        description: "Fleurs de cerisier",
        slug: "sylvie_cerisier"
    },
    {
        author: "Jean-Michel",
        description: "Fleurs violette et pissenlits",
        slug: "jm_pissenlits"
    },
    {
        author: "Sylvie",
        description: "Grosses fleurs violettes",
        slug: "sylvie_grosses_fleurs_violettes"
    },
    {
        author: "Ludovic",
        description: "Fleurs violettes",
        slug: "ludo_violettes"
    },
    {
        author: "Ludovic",
        description: "Fleurs violettes",
        slug: "ludo_violettes_2"
    },
    {
        author: "Jean-Michel",
        description: "Fleur et rosée",
        slug: "jm_rosee"
    },
    {
        author: "Julien",
        description: "Abeille butinant",
        slug: "julien_abeille"
    }
];

export default { allPhotos };