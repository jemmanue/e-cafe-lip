#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ORIGIN_DIR=${DIR}/raw_photos
TARGET_DIR=${DIR}/src/assets/photos
THUMBNAILS_DIR=${TARGET_DIR}/thumbnails
ARCHIVE_DIR=${DIR}/fullsize_photos

function processFiles {
  FILES=$1
  EXT_SIZE=$2
  if (ls ${FILES} 1> /dev/null 2>&1) # Check if there is any
  then
    for f in $FILES
    do
      echo "Processing ${f}"
      BASE_PHOTO_PATH=$( echo $f | awk -v EXT_SIZE="$EXT_SIZE" '{ print substr( $0, 0, length($0)-EXT_SIZE ) }')
      SLUG=$( basename $BASE_PHOTO_PATH )
      convert $f -quality 85% -resize 2048x2048\> ${TARGET_DIR}/${SLUG}.jpg
      convert $f -quality 85% -resize 512x512\> ${THUMBNAILS_DIR}/${SLUG}.jpg
      mv $f ${ARCHIVE_DIR}
    done
  fi
}

processFiles "${ORIGIN_DIR}/*.jpg" 4
processFiles "${ORIGIN_DIR}/*.JPG" 4
processFiles "${ORIGIN_DIR}/*.jpeg" 5
processFiles "${ORIGIN_DIR}/*.JPEG" 5
processFiles "${ORIGIN_DIR}/*.png" 4
processFiles "${ORIGIN_DIR}/*.PNG" 4